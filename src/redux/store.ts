import {createStore, combineReducers, compose, applyMiddleware } from "redux";
import logger from 'redux-logger';
import {
  RouterState,
  connectRouter,
  routerMiddleware,
  CallHistoryMethodAction
} from 'connected-react-router';
import { createBrowserHistory } from 'history';
import thunk, { ThunkDispatch } from 'redux-thunk'
import { IDashBoardState } from "./dashboard/state";
import { DashboardReducer } from "./dashboard/reducer";
import { IDashboardAction } from "./dashboard/action";
import { IUserAction } from "./user/action";
import { UserReducer } from "./user/reducer";
import { IUserState } from "./user/state";
import { ReportReducer } from "./report/reducer";
import { IReportAction } from "./report/action";
import { IReportState } from "./report/state";

export type IRootThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>

export const history = createBrowserHistory();

export type IRootState = {
  dashboard: IDashBoardState
  report: IReportState
  user: IUserState
  router: RouterState
}

export type IRootAction = IDashboardAction | IReportAction | IUserAction | CallHistoryMethodAction;

const rootReducer = combineReducers<IRootState>({
  dashboard: DashboardReducer,
  report: ReportReducer,
  user: UserReducer,
  router: connectRouter(history)
})

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore<IRootState,IRootAction,{},{}>
  (
    rootReducer, 
    composeEnhancers(
      applyMiddleware(logger),
      applyMiddleware(routerMiddleware(history)),
      applyMiddleware(thunk),
    )
  );
export default store;