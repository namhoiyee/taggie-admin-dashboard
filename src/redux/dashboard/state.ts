export type IDashBoardState = {
  statData: {
    dailyUser: number[] | null[], 
    dailyRegistration: number[] | null[], 
    dailyMatch: number[] | null[], 
    dailyConsumption: number[] | null[]},
  chartType: "Bar" | "Spline" | "Pie",
  period: number,
  statDataType: "dailyUser" | "dailyRegistration" | "dailyMatch" | "dailyConsumption",
}

export const initialState: IDashBoardState = {
  statData: {
    dailyUser: [null], 
    dailyRegistration: [null], 
    dailyMatch: [null], 
    dailyConsumption: [null]},
  chartType: "Bar",
  period: 7,
  statDataType: "dailyUser",
}