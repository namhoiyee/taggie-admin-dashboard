export function changeType(chart: "Bar" | "Spline" | "Pie"){
  return {
    type: '@@Dashboard/change-type' as const,
    chart
  }
}

export function changePeriod(period: number){
  return {
    type: '@@Dashboard/change-period' as const,
    period
  }
}

export function getData(data: any){
  return {
    type: '@@Dashboard/get-data' as const,
    data
  }
}

export function changeDataType(dataType: "dailyUser" | "dailyRegistration" | "dailyMatch" | "dailyConsumption"){
  return {
    type: '@@Dashboard/change-data-type' as const,
    dataType
  }
}

export type IDashboardAction = 
  | ReturnType<typeof changeType>
  | ReturnType<typeof changePeriod>
  | ReturnType<typeof getData>
  | ReturnType<typeof changeDataType>