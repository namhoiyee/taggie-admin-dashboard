import produce from 'immer';
import {IDashboardAction} from './action';
import {IDashBoardState, initialState} from './state';

export const DashboardReducer = produce((
  state: IDashBoardState = initialState,
  action: IDashboardAction,
): void=>{
  switch (action.type){

    case '@@Dashboard/change-type':{
      try {
        state.chartType = action.chart
        return
      } catch (error) {

        return
      }
    }
    
    case '@@Dashboard/change-period':{
      try {
        state.period = action.period
        console.log('finally :',state);
        
        return
      } catch (error) {

        return
      }
    }

    case '@@Dashboard/get-data':{
      try {
        state.statData = {...action.data}
        return
      } catch (error) {

        return
      }
    }

    case '@@Dashboard/change-data-type':{
      try {
        state.statDataType = action.dataType
        return
      } catch (error) {

        return
      }
    }
  }
}, initialState)