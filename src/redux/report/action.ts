export function changeReportSearchType(search: "reportID" | "email" | "category" | "content"){
  return {
    type: '@@Report/change-search-type' as const,
    search
  }
}

export function getReportData(reportData: any){
  return {
    type: '@@Report/get-data' as const,
    reportData
  }
}

export function handleReportDataAfterSearch(keyword: string){
  return {
    type: '@@Report/handle-data-after-search' as const,
    keyword
  }
}

export type IReportAction = 
| ReturnType<typeof changeReportSearchType>
| ReturnType<typeof getReportData>
| ReturnType<typeof handleReportDataAfterSearch>
