import { initialState } from './state'
import { IRootThunkDispatch } from '../store';
import { getReportData } from './action';

const { REACT_APP_API_SERVER } = process.env

export function getReportDataThunk(){
  
  return async (dispatch: IRootThunkDispatch) => {
    // const res = await fetch(`${REACT_APP_API_SERVER}/getUserData`);
    // const result = await res.json()

    let reportData = [
      {
        reportID: 13,
        reporter: "Leo",
        email: "leo@leo.com",
        category: "inconvenience design",
        target: "target",
        content: "13 The UI design is so inconvenient, the XXX button is easy to be mis-clicked. Please update the UI to avoid this.",
        isRead: "no",
        isReplied: "no",
        date: "Jun 06 2021"
      },
      {
        reportID: 12,
        reporter: "sfsdf",
        email: "sczxczk123@123.com",
        category: "fsdf Harassment",
        target: "dfhdhdf",
        content: "12 She sent some inappropriate text and photo to me.",
        isRead: "yes",
        isReplied: "no",
        date: "Jun 06 2021"
      },
      {
        reportID: 11,
        reporter: "dsfsdfsd",
        email: "fxzczk222@123.com",
        category: "sadsad Harassment",
        target: "erwrwds",
        content: "11 She sent some inappropriate text and photo to me.",
        isRead: "no",
        isReplied: "no",
        date: "Jun 06 2021"
      },
      {
        reportID: 10,
        reporter: "erer",
        email: "eccck123@123.com",
        category: "rewr wHarassment",
        target: "zczx",
        content: "10 She sent some inappropriate text and photo to me.",
        isRead: "yes",
        isReplied: "yes",
        date: "Jun 05 2021"
      },
      {
        reportID: 9,
        reporter: "asdasd",
        email: "qczxck222@123.com",
        category: "uty Harassment",
        target: "kjhkg",
        content: "9 She sent some inappropriate text and photo to me.",
        isRead: "yes",
        isReplied: "yes",
        date: "Jun 05 2021"
      },
      {
        reportID: 8,
        reporter: "nvnv",
        email: "fxxk123@123.com",
        category: "yrty Harassment",
        target: "Michhgjghjael",
        content: "8 She sent some inappropriate text and photo to me.",
        isRead: "no",
        isReplied: "no",
        date: "Jun 04 2021"
      },
      {
        reportID: 7,
        reporter: "jghjg",
        email: "vcask222@123.com",
        category: "trtre Harassment",
        target: "ytuytu",
        content: "7 She sent some inappropriate text and photo to me.",
        isRead: "yes",
        isReplied: "yes",
        date: "Jun 04 2021"
      },
      {
        reportID: 6,
        reporter: "zxczxc",
        email: "tasdk123@123.com",
        category: "qwe Harassment",
        target: "adasd",
        content: "6 sent some inappropriate text and photo to me.",
        isRead: "yes",
        isReplied: "no",
        date: "Jun 04 2021"
      },
      {
        reportID: 5,
        reporter: "qweeq",
        email: "rxzck222@123.com",
        category: "rewr Harassment",
        target: "zxvzxvz",
        content: "5 sent some inappropriate text and photo to me.",
        isRead: "yes",
        isReplied: "yes",
        date: "Jun 03 2021"
      },
      {
        reportID: 4,
        reporter: "zvzxv",
        email: "zxcek123@123.com",
        category: "ewr Harassment",
        target: "qwrqdq",
        content: "4 sent some inappropriate text and photo to me.",
        isRead: "yes",
        isReplied: "yes",
        date: "Jun 03 2021"
      },
      {
        reportID: 3,
        reporter: "qerqer",
        email: "scxzk222@123.com",
        category: "asdas Harassment",
        target: "dfhdfh",
        content: "3 sent some inappropriate text and photo to me.",
        isRead: "yes",
        isReplied: "yes",
        date: "Jun 02 2021"
      },
      {
        reportID: 2,
        reporter: "hfdhdf",
        email: "ckasd123@123.com",
        category: "asdas Harassment",
        target: "asfasf",
        content: "2 sent some inappropriate text and photo to me.",
        isRead: "yes",
        isReplied: "yes",
        date: "Jun 01 2021"
      },
      {
        reportID: 1,
        reporter: "Kesafvin2",
        email: "hksad222@123.com",
        category: "gdsda Harassment",
        target: "asfsafsaf",
        content: "1 sent some inappropriate text and photo to me.",
        isRead: "yes",
        isReplied: "yes",
        date: "Jun 01 2021"
      },
    ];
    dispatch(getReportData(reportData))
  }
}