import produce from 'immer';
import {IReportAction} from './action';
import {IReportState, initialState} from './state';

function updateSearchResult(state: IReportState, keyword: string){
  let newArray: {
    reportID: number,
    reporter: string,
    email: string,
    category: string,
    target: string,
    content: string,
    isRead: string,
    isReplied: string,
    date: string
  }[] = [];
  for (let i = 0; i < state.reportData.length; i++){
    let type = state.reportSearchType
    let target: any = state.reportData[i][type]?.toString()
    if (target.toUpperCase().includes(keyword.toUpperCase())){
      console.log(target.toUpperCase().includes(keyword.toUpperCase()))
      let item = {
        reportID: state.reportData[i]["reportID"],
        reporter: state.reportData[i]["reporter"],
        email: state.reportData[i]["email"],
        category: state.reportData[i]["category"],
        target: state.reportData[i]["target"],
        content: state.reportData[i]["content"],
        isRead: state.reportData[i]["isRead"],
        isReplied: state.reportData[i]["isReplied"],
        date: state.reportData[i]["date"]
      }
      newArray.push(item)
    }
  }
  console.log("newArray after search: ", newArray)
  state.reportDataAfterSearch = newArray.slice()
  return
}

export const ReportReducer = produce((
  state: IReportState = initialState,
  action: IReportAction,
): void =>{
  switch (action.type){

    case '@@Report/change-search-type':{
      try {
        state.reportSearchType = action.search
        return
      } catch (error) {

        return
      }
    }
    
    case '@@Report/get-data':{
      try {
        state.reportData = action.reportData.slice()
        return
      } catch (error) {

        return
      }
    }

    case '@@Report/handle-data-after-search':{
      try {
        state.reportSearchKeyword = action.keyword
        updateSearchResult(state, action.keyword)
        return
      } catch (error) {

        return
      }
    }

  }
}, initialState)