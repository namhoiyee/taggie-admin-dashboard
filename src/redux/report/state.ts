export type IReportState = {
  reportData: {
    reportID: number,
    reporter: string,
    email: string,
    category: string,
    target: string,
    content: string,
    isRead: string,
    isReplied: string,
    date: string
  }[],
  reportSearchType: "reportID" | "email" | "category" | "content",
  reportSearchKeyword: string,
  reportDataAfterSearch: {
    reportID: number,
    reporter: string,
    email: string,
    category: string,
    target: string,
    content: string,
    isRead: string,
    isReplied: string,
    date: string
  }[],
  // unreadReportCounter: number
}

export const initialState: IReportState = {
  reportData: [{
    reportID: 0,
    reporter: "reporter",
    email: "email",
    category: "category",
    target: "target",
    content: "content",
    isRead: "no",
    isReplied: "no",
    date: "date"
  }],
  reportSearchType: "reportID",
  reportSearchKeyword: "",
  reportDataAfterSearch: [{
    reportID: 0,
    reporter: "reporter",
    email: "email",
    category: "category",
    target: "target",
    content: "content",
    isRead: "no",
    isReplied: "no",
    date: "date"
  }],
  // unreadReportCounter: 0
}