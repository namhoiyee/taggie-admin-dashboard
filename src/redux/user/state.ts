export type IUserState = {
  userData: {
    userID: number,
    username: string,
    email: string,
    coins: number,
    lastOnline: string,
  }[],
  userSearchType: "userID" | "username" | "email",
  userSearchKeyword: string,
  userDataAfterSearch: {
    userID: number,
    username: string,
    email: string,
    coins: number,
    lastOnline: string,
  }[],
  userDetail: {
    id: number;
    phone_num: string | null;
    registration_status: number;
    google_access_token: string | null;
    birthday: string | null;
    gender_id: number | null;
    created_at: string;
    text_description: string | null;
    voice_description: string | null;
    isBlocked: boolean;
    isFake: boolean;
    user_photos: [{
      url: string | null,
      description: string | null,
      created_at: string | null
    }]
  }
}

export const initialState: IUserState = {
  userData: [{
    userID: 0,
    username: "username",
    email: "email",
    coins: 0,
    lastOnline: "lastOnline",
  }],
  userSearchType: "userID",
  userSearchKeyword: "",
  userDataAfterSearch: [{
    userID: 0,
    username: "username",
    email: "email",
    coins: 0,
    lastOnline: "lastOnline",
  }],
  userDetail: {
    id: 0,
    phone_num: null,
    registration_status: -999,
    google_access_token: null,
    birthday: null,
    gender_id: null,
    created_at: "000 00 0000",
    text_description: null,
    voice_description: null,
    isBlocked: false,
    isFake: false,
    user_photos: [{
      url: null,
      description: null,
      created_at: null
    }]
  }
}