import produce from 'immer';
import {IUserAction} from './action';
import {IUserState, initialState} from './state';

function updateSearchResult(state: IUserState, keyword: string){
  let newArray: {
    userID: number,
    username: string,
    email: string,
    coins: number,
    lastOnline: string,
  }[] = [];
  for (let i = 0; i < state.userData.length; i++){
    let type = state.userSearchType
    let target: any = state.userData[i][type]?.toString()
    if (target.toUpperCase().includes(keyword.toUpperCase())){
      console.log(target.toUpperCase().includes(keyword.toUpperCase()))
      let item = {
        userID: state.userData[i]["userID"],
        username: state.userData[i]["username"],
        email: state.userData[i]["email"],
        coins: state.userData[i]["coins"],
        lastOnline: state.userData[i]["lastOnline"],
      }
      newArray.push(item)
    }
  }
  console.log("newArray after search: ", newArray)
  state.userDataAfterSearch = newArray.slice()
  return
}

export const UserReducer = produce((
  state: IUserState = initialState,
  action: IUserAction,
): void=>{
  switch (action.type){

    case '@@User/change-search-type':{
      try {
        state.userSearchType = action.search
        return
      } catch (error) {

        return
      }
    }

    case '@@User/get-data':{
      try {
        state.userData = action.userData.slice()
        return
      } catch (error) {

        return
      }
    }

    case '@@User/handle-data-after-search':{
      try {
        state.userSearchKeyword = action.keyword
        updateSearchResult(state, action.keyword)
        return
      } catch (error) {

        return
      }
    }

    case '@@User/get-user-detail':{
      try {
        state.userDetail = {...action.userDetail}
        return
      } catch (error) {

        return
      }
    }
  }
}, initialState)