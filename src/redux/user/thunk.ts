import { initialState } from './state';
import { IRootThunkDispatch } from '../store';
import { getUserData, getUserDetail } from './action';

const { REACT_APP_API_SERVER } = process.env

export function getUserDataThunk(){
  
  return async (dispatch: IRootThunkDispatch) => {
    // const res = await fetch(`${REACT_APP_API_SERVER}/getUserData`);
    // const result = await res.json()

    interface Data {
      userID: number,
      username: string,
      email: string,
      coins: number,
      lastOnline: string,
    }
  
    function createData(
      userID: number,
      username: string,
      email: string,
      coins: number,
      lastOnline: string,
    ): Data {
      return { userID, username, email, coins, lastOnline }
    }

    let userData = [
      createData(26, "zafasfasfz", "zasfasfz@123.com", 100, "Jun 05 2021"),
      createData(25, "yysafasf", "ysafasfy@123.com", 51, "Jun 05 2021"),
      createData(24, "xsafasfx", "xasfasfasfx@123.com", 24, "Jun 05 2021"),
      createData(23, "ww", "ww@123.com", 24, "Jun 01 2021"),
      createData(22, "vasfasfsav", "vasfasfasv@123.com", 49, "Jun 02 2021"),
      createData(21, "uu", "uu@123.com", 87, "Jun 03 2021"),
      createData(20, "tasfasft", "tasfasfast@123.com", 37, "Jun 05 2021"),
      createData(19, "ss", "ss@123.com", 94, "Jun 05 2021"),
      createData(18, "rr", "rr@123.com", 65, "Jun 04 2021"),
      createData(17, "qasfasfasfq", "qasfasfasq@123.com", 98, "Jun 04 2021"),
      createData(16, "pp", "pp@123.com", 81, "Jun 05 2021"),
      createData(15, "oo", "oo@123.com", 9, "Jun 03 2021"),
      createData(14, "nn", "nn@123.com", 63, "Jun 05 2021"),
      createData(13, "masfasfm", "msafasfm@123.com", 67, "Jun 04 2021"),
      createData(12, "ll", "ll@123.com", 51, "Jun 05 2021"),
      createData(11, "kk", "kk@123.com", 24, "Jun 01 2021"),
      createData(10, "jasfasfasj", "jsafasj@123.com", 24, "Jun 02 2021"),
      createData(9, "iasfasfi", "iasfasfsafasi@123.com", 49, "Jun 05 2021"),
      createData(8, "hh", "hh@123.com", 87, "Jun 05 2021"),
      createData(7, "gsafasg", "gasfasfasfg@123.com", 37, "Jun 04 2021"),
      createData(6, "fasfsaff", "fafsafasff@123.com", 94, "Jun 03 2021"),
      createData(5, "ee", "ee@123.com", 65, "Jun 02 2021"),
      createData(4, "dasfsafd", "dsafasfasd@123.com", 98, "Jun 01 2021"),
      createData(3, "cc", "cc@123.com", 81, "Jun 04 2021"),
      createData(2, "basfasfb", "bsafasfsab@123.com", 9, "Jun 03 2021"),
      createData(1, "aasfsafa", "aasfasfasfasa@123.com", 63, "Jun 05 2021"),
    ];
    dispatch(getUserData(userData))
  }
}

export function getUserDetailThunk(userID: number){

  return async (dispatch: IRootThunkDispatch) => {
    // const res = await fetch(`${REACT_APP_API_SERVER}/getUserDetail`, {

    // });
    // const result = await res.json()
    let detail
    if (userID === 1){
      detail = {
        id: 1,
        phone_num: "12345678",
        registration_status: 3,
        google_access_token: null,
        birthday: "Jan 01 1999",
        gender_id: 2,
        created_at: "Jun 04 2021",
        text_description: "There is a special attractive to make us meet under the same sky.",
        voice_description: null,
        isBlocked: false,
        isFake: false,
        user_photos: [
          {
            url: "logo192.png",
            description: "profile",
            created_at: "Jun 04 2021"
          },
          {
            url: "logo512.png",
            description: "photo-1",
            created_at: "Jun 04 2021"
          },
          {
            url: "Logo3.svg",
            description: "photo-2",
            created_at: "Jun 05 2021"
          }
        ]
      }
    } else if (userID === 2){
      detail = {
        id: 2,
        phone_num: "22225678",
        registration_status: 2,
        google_access_token: "google token here",
        birthday: "Feb 02 2002",
        gender_id: 1,
        created_at: "Jun 04 2020",
        text_description: "No one like me.",
        voice_description: "http://f3.htqyy.com/play9/143/mp3/5",
        isBlocked: true,
        isFake: false,
        user_photos: [
          {
            url: "Logo2.svg",
            description: "profile",
            created_at: "Jun 04 2021"
          },
          {
            url: "Logo.svg",
            description: "photo-1",
            created_at: "Jun 05 2021"
          },
          {
            url: "Logo3.svg",
            description: "photo-2",
            created_at: "Jun 06 2021"
          }
        ]
      }
    } else {
      detail = initialState.userDetail
    }
    
    dispatch(getUserDetail(detail))
  }
}