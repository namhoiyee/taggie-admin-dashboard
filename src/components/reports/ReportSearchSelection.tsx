import { createStyles, FormControl, InputLabel, makeStyles, Select, Theme } from '@material-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { changeReportSearchType } from '../../redux/report/action';


const useStyles = makeStyles((theme: Theme) =>
createStyles({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}),
);

export default function ReportSearchSelection (){

  const classes = useStyles();
  const dispatch = useDispatch();

  const handleChange = (event: React.ChangeEvent<{ name?: string; value: any }>) => {
    let num = parseInt(event.target.value)
    if (num === 0){
      dispatch(changeReportSearchType("reportID"))
    } else if (num === 1){
      dispatch(changeReportSearchType("email"))
    } else if (num === 2){
      dispatch(changeReportSearchType("category"))
    } else {
      dispatch(changeReportSearchType("content"))
    }
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <Select
        native
        onChange={handleChange}
        placeholder="Bar Chart"
      >
        <option value={0}>Report ID</option>
        <option value={1}>Email</option>
        <option value={2}>Category</option>
        <option value={3}>Content</option>
      </Select>
    </FormControl>
  )
}