import { Box, Card, CardContent, InputAdornment, SvgIcon, TextField } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import React from 'react';
import { useDispatch } from 'react-redux';
import { handleReportDataAfterSearch } from '../../redux/report/action';
import ReportSearchSelection from './ReportSearchSelection';

export default function ReportSearch(){
  const dispatch = useDispatch();

  function searchReport(event: any){
    console.log(event.target.value)
    dispatch(handleReportDataAfterSearch(event.target.value))
  }

  return (
    <Box mb="1.5rem">
      <Card variant="outlined">
        <CardContent>
          <Box maxWidth="500px" display="flex" alignItems="center">
            <ReportSearchSelection />
            <TextField
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SvgIcon
                      fontSize="small"
                      color="action"
                    >
                      <SearchIcon />
                    </SvgIcon>
                  </InputAdornment>
                )
              }}
              placeholder="Search report"
              variant="outlined"
              onKeyUp={searchReport}
            />
          </Box>
        </CardContent>
      </Card>
    </Box>
  )
}