import { Box, Card, CardContent, InputAdornment, SvgIcon, TextField } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import React from 'react';
import { useDispatch } from 'react-redux';
import { handleUserDataAfterSearch } from '../../redux/user/action';
import UserSearchSelection from './UserSearchSelection';

export default function UserSearch(){
  const dispatch = useDispatch();

  function searchUser(event: any){
    console.log(event.target.value)
    dispatch(handleUserDataAfterSearch(event.target.value))
  }

  return (
    <Box mb="1.5rem">
      <Card variant="outlined">
        <CardContent>
          <Box maxWidth="500px" display="flex" alignItems="center">
            <UserSearchSelection />
            <TextField
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SvgIcon
                      fontSize="small"
                      color="action"
                    >
                      <SearchIcon />
                    </SvgIcon>
                  </InputAdornment>
                )
              }}
              placeholder="Search report"
              variant="outlined"
              onKeyUp={searchUser}
            />
          </Box>
        </CardContent>
      </Card>
    </Box>
  )
}