import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import CircularProgress from '@material-ui/core/CircularProgress';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { IconButton } from "@material-ui/core";
import { Link } from "react-router-dom";
import { getUserDetailThunk } from "../../redux/user/thunk";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import BlockIcon from '@material-ui/icons/Block';
import UndoIcon from '@material-ui/icons/Undo';
import { push } from "connected-react-router";

export default function UserDetail(){

  const users = useSelector((state: IRootState) => state.user.userData);
  const dispatch = useDispatch();
  
  const id = parseInt(window.location.href.slice(window.location.href.search("Users/")).slice(6))
  useEffect(()=>{
    dispatch(getUserDetailThunk(id))
  }, [])
  
  let user;
  for (let i = 0; i < users.length; i++){
    if (users[i].userID === id){
      user = users[i]
      break
    }
  }

  const userDetail = useSelector((state: IRootState) => state.user.userDetail);  

  function userPhoto(){
    let userPhotoDom = []
    for (let i = 0; i < userDetail.user_photos.length; i++){
      let photo = (<img src={"/" + userDetail.user_photos[i].url as string}></img>)
      userPhotoDom.push(photo)
    }
    return userPhotoDom
  }
  

  return (
    <div style={{width: "100%"}}>
      <IconButton title="Return" component={Link} to="/Users"><ArrowBackIcon /></IconButton>
      {
        userDetail.isBlocked === true
        ?<IconButton title="Undo" edge="end"><UndoIcon /></IconButton>
        :<IconButton title="Block" edge="end"><BlockIcon /></IconButton>
      }
      <IconButton title="Delete"><DeleteForeverIcon /></IconButton>
      <h1>User Information</h1>
      <p>User ID: {user?.userID}</p>
      <p>Username: {user?.username}</p>
      <p>Email: {user?.email}</p>
      <p>Coins: {user?.coins}</p>
      <p>Last Online: {user?.lastOnline}</p>
      {userDetail.id === 0 ?<CircularProgress /> 
      :<>
        <p>Phone Number: {userDetail.phone_num}</p>
        <p>Registration status: {userDetail.registration_status}</p>
        <p>Google access token: {userDetail.google_access_token !== null ? "Yes" : "No"}</p>
        <p>Birthday: {userDetail.birthday}</p>
        <p>Gender: {userDetail.gender_id === 1 ? "Female" : <>{userDetail.gender_id === 2 ? "Male" : "Other"}</>}</p>
        <p>Registration Date: {userDetail.created_at}</p>
        <p>Text Description: {userDetail.text_description}</p>
        <p>Voice Description: {userDetail.voice_description !== null ? <audio controls src={userDetail.voice_description}></audio> : "No"}</p>
        <p>Status: {userDetail.isBlocked === false ? "Normal" : "Blocked"}</p>
        <p>Robot: {userDetail.isFake === false ? "No" : "Yes"}</p>
        <div>
          <p>Photos:</p>
          {userPhoto()}
        </div>
      </>
      }
    </div>
  )
}