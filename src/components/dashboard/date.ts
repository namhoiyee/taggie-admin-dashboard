export default function dateConversion(number: number): string{
  let date: any = new Date();
  let newDate = new Date(date - (number*86400000))
  return ((newDate.toDateString()).slice(4, -5))
}