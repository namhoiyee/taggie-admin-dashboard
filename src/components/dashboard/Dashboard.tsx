import { createStyles, CssBaseline, makeStyles, Theme, Typography } from '@material-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { getDataThunk } from '../../redux/dashboard/thunk';
import ChartSelection from './ChartSelection';
import './Dashboard.css';
import InfoChart from './InfoChart';
import PeriodSelection from './PeriodSelection';
import UserInfo from './UserInfo';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

export default function Dashboard(){

  const classes = useStyles();
  const dispatch = useDispatch();

  React.useEffect(()=>{
    dispatch(getDataThunk())
  }, [])
  
  return (
    <>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <div className="container">
          <div className="userCard">
            <UserInfo cardName="dailyUser"/>
            <UserInfo cardName="dailyRegistration"/>
            <UserInfo cardName="dailyMatch"/>
            <UserInfo cardName="dailyConsumption"/>
          </div>
          <div className="selector">
            <ChartSelection/>
            <PeriodSelection/>
          </div>
          <div>
            <InfoChart />
          </div>
        </div>
      </main>
    </>
  )
}