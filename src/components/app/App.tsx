import { ConnectedRouter } from 'connected-react-router';
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { history } from '../../redux/store';
import Dashboard from '../dashboard/Dashboard';
import Navbar from '../navbar/Navbar';
import NoMatch from '../noMatch/NoMatch';

export default function App() {
  return (
    <ConnectedRouter history={history}>
      <Navbar/>
      
    </ConnectedRouter>
  );
}